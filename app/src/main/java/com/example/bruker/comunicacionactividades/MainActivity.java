package com.example.bruker.comunicacionactividades;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Button b;
    EditText v2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        b=(Button)findViewById(R.id.verificar);

        /**
         * Clase ANONIMA, al boton "b" le estamos aplicando un metodo(setOnClickListener)
         * y a este metodo le estamos pasando una interfaz
         * En el layout, no hace falta indicarle nada al metodo onClick del boton, se queda sin nada
         *
         * En el intent, le tenemos que indicar desde donde (MainActivity.this)
         * hasta donde (SecondActivity.class).
         *
         * Como intent está dentro de una clase anonima, le tenemos que concretizar que this es
         * del MainActivity, si dejaramos el this a secas, no funciona (aunque si lo pones en un metodo
         * normal, tipico, si funciona)
         */
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v2=(EditText) findViewById(R.id.edit);
                String s = v2.getText().toString();
                Intent i = new Intent(MainActivity.this,SecondActivity.class);
                i.putExtra("usuario", s);
                //tenemos que usar este metodo si esperamos un resultado de la otra actividad
                //el requestCode es por lo visto, cualquier codigo, lo importante es que sea de tipo
                //entero
                startActivityForResult(i, 1234);


            }
        });

        /**
         * Lo de arriba sería equivalente a esto, pero aplicado a un boton
         */
        /*View.OnClickListener s = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView v = (TextView)findViewById(R.id.resultado);

        /**
         *  HACEMOS ESTO para de alguna manera identificar al responsable de enviar estos datos
         *  Ya que ( no estoy seguro) podria ser que tuvieramos varias actividades, y a cada actividad
         *  la iniciaramos con un startActivityForResult, pero al ponerle un codigo a cada una (el que sea)
         *  luego, en el metodo onActivityResult identificamos cual esta mandando los resultados
         */

        if(requestCode==1234 && resultCode==RESULT_OK){
            String s = data.getStringExtra("opcion");
            v.setText("Resultado: "+s);
        }


    }
}
