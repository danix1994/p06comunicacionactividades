package com.example.bruker.comunicacionactividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    Button b;
    Button b2;
    TextView v2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        b=(Button)findViewById(R.id.aceptar);
        b2=(Button)findViewById(R.id.rechazar);
        v2=(TextView)findViewById(R.id.saludo);

        Bundle recogida = getIntent().getExtras();
        String s = recogida.getString("usuario");
        v2.setText("Hola "+s+ ", ¿Aceptas las condiciones?");

        View.OnClickListener escuchador = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button boton_pulsado=(Button)v; // --> asi obtenemos el boton pulsado
                String s = boton_pulsado.getText().toString();

                Intent i = null;

                if(s.equals("ACEPTAR")){
                    i=new Intent();
                    i.putExtra("opcion", "Aceptado");
                    setResult(RESULT_OK, i);
                    finish();
                }else{
                    i=new Intent();
                    i.putExtra("opcion", "Rechazado");
                    setResult(RESULT_OK, i);
                    finish();
                }

            }
        };

        b.setOnClickListener(escuchador);
        b2.setOnClickListener(escuchador);



    }


}
